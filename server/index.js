const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const fs = require('fs');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.set('io', io);
global.app = app;

app.use(express.static(__dirname + '/public'));

fs.readdirSync(__dirname.concat('/handlers')).forEach(function(fileName) {
    const name = __dirname.concat('/handlers/', fileName);
    const stat = fs.statSync(name);
    
    if (stat.isFile()) {
       require(name);
    }
});

app.on('error', (err) => {
    console.error(err);
});

http.listen(8000, function(){
    console.log('listening on *:8000');
});
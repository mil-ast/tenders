const multer = require('multer');
const upload = multer({dest: './uploads'});
const conn = require('../db');

/*
    загрузка файлов
*/
app.post('/api/lots/upload', upload.any(), function (req, res) {
    const lot_id = req.body.lot_id|0;
    if (lot_id === 0) {
        res.sendStatus(400);
        return;
    }

    if (!req.files || !Array.isArray(req.files) || req.files.length == 0) {
        res.sendStatus(400);
        return;
    }

    let sql_val = [];
    let values = [];
    let results = [];
    for (let i = 0; i < req.files.length; i++) {
        sql_val.push('(?,?,?,?)');
        values.push(
            lot_id,
            req.files[i].originalname,
            req.files[i].filename,
            req.files[i].mimetype,
        );

        results.push({
            id : 0,
            lot_id : lot_id,
            name : req.files[i].originalname,
        });
    }

    const query_sql = 'INSERT INTO `files` (`lot_id`,`name`,`dist_name`,`mimetype`) VALUES '.concat(sql_val.join(','));
    conn.query(query_sql, values, (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }
        let id = result.insertId;

        for (let i = 0; i < results.length; i++) {
            results[i].id = id++;
        }

        res.status(201).json(results);
    });
});
const conn = require('../db');

/**
 * сохранение
 */
app.put('/api/kp', function (req, res) {
    const lot_id = req.body.lot_id|0;
    if (!lot_id) {
        res.sendStatus(400);
        return;
    }

    const query_sql = 'UPDATE `lots` SET `kp_name`=?,`kp_delivery_time`=?,`kp_delivery_term`=?,`kp_payment_term`=? WHERE `id`=?';
    query_params = [
        req.body.kp_name,
        req.body.kp_delivery_time,
        req.body.kp_delivery_term,
        req.body.kp_payment_term,
        lot_id,
    ];

    conn.query(query_sql, query_params, (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        res.status(202).json({
            lot_id : lot_id,
            kp_name : req.body.kp_name,
            kp_delivery_time : req.body.kp_delivery_time,
            kp_delivery_term : req.body.kp_delivery_term,
            kp_payment_term : req.body.kp_payment_term,
        });
    });
});

/**
 * прочитать все позиции
 */
app.get('/api/kp/positions/', function (req, res) {
    const lot_id = req.query.lot_id|0;
    if (!lot_id) {
        res.sendStatus(404);
        return;
    }

    const query_sql = 'SELECT * FROM `kp_positions` WHERE `lot_id`=?';

    conn.query(query_sql, [lot_id], (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        res.send(result);
    });
});
/**
 * добавление позиции
 */
app.post('/api/kp/positions', function (req, res) {
    if (!req.body.lot_id) {
        res.sendStatus(400);
        return;
    }

    const query_sql = 'INSERT INTO `kp_positions` SET ?';
    const query_params = {
        lot_id : req.body.lot_id,
        name : req.body.name,
        cnt : req.body.cnt,
        unit : req.body.unit,
        price : req.body.price,
        delivery : req.body.delivery,
    };
    conn.query(query_sql, query_params, (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        res.status(201).json({
            id : result.insertId,
            name : query_params.name,
            cnt : query_params.cnt,
            unit : query_params.unit,
            price : query_params.price,
            delivery : query_params.delivery,
        });
    });
});

/**
 * редактирование позиции
 */
app.put('/api/kp/positions', function (req, res) {
    if (!req.body.id) {
        res.sendStatus(404);
        return;
    }

    const query_sql = 'UPDATE `kp_positions` SET `name`=?,`cnt`=?,`unit`=?,`price`=?,`delivery`=? WHERE `id`=?';
    const query_params = [
        req.body.name,
        req.body.cnt,
        req.body.unit,
        req.body.price,
        req.body.delivery,
        req.body.id|0,
    ];

    conn.query(query_sql, query_params, (err) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        res.status(202).json({
            id : req.body.id|0,
            name : req.body.name,
            cnt : req.body.cnt,
            unit : req.body.unit,
            price : req.body.price,
            delivery : req.body.delivery,
        });
    });
});

/**
 * удаление позиции
 */
app.delete('/api/kp/positions', function (req, res) {
    const id = req.query.id|0;

    if (!id) {
        res.sendStatus(404);
        return;
    }

    const query_sql = "DELETE FROM `kp_positions` WHERE `id`=?";
    var query_data = [ id ];

    conn.query(query_sql, query_data, (err) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        res.sendStatus(204);
    });
});
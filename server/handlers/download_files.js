const conn = require('../db');
const path = require('path');
const fs = require('fs');

app.get('/api/download/:id', function (req, res) {
    conn.query("SELECT `name`,`dist_name`,`mimetype` FROM `files` WHERE `id`=?", [req.params.id], (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        if (!Array.isArray(result) || result.length === 0) {
            res.sendStatus(404);
            return;
        }

        const data = result[0];
        const filePath = path.resolve(`./uploads/${data.dist_name}`);

        fs.exists(filePath, function(exists){
            if (exists) {
                res.writeHead(200, {
                  'Content-Type': data.mimetype,
                  'Content-Disposition': `attachment; filename=${encodeURIComponent(data.name)}`
                });

                fs.createReadStream(filePath).pipe(res);
            } else {
                res.writeHead(404, {"Content-Type": "text/plain"});
                res.end("ERROR: File does not exist");
            }
        })        
    });
});

app.delete('/api/files/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.sendStatus(404);
        return;
    }

    conn.query("SELECT `dist_name` FROM `files` WHERE `id`=?", [id], (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        if (!Array.isArray(result) || result.length === 0) {
            res.sendStatus(404);
            return;
        }

        const data = result[0];
        const filePath = path.resolve(`./uploads/${data.dist_name}`);

        fs.exists(filePath, function(exists){
            if (exists) {     
                fs.unlink(filePath, (err) => {
                    if (err !== null) {
                        console.err(err);
                    }
                });
            }

            conn.query('DELETE FROM `files` WHERE `id`=?', [id], () => {
                res.sendStatus(204);
            });
        }) 
    });
});
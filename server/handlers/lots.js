const conn = require('../db');
const fs = require('fs');

app.get('/api/lots', function (req, res) {
    const pageIndex = req.query.pageIndex|0;
    const count = req.query.count|0;
    const filter_region = req.query.region||null;
    const filter_manufacturer = req.query.manufacturer||null;
    const filter_stage = req.query.stage||null;

    let params = [];

    let query_stage = '`lots`.`stage` NOT IN(\'failed\',\'finish\') OR `lots`.`stage` IS NULL';

    let query_order = 'ORDER BY IFNULL(`lots`.`date_review`,`lots`.`date_end_auctions`)';
    const direction = req.query.orderDirection === 'asc' ? 'ASC' : 'DESC';

    if (req.query.orderActive) {
        switch(req.query.orderActive) {
        case 'number':case 'region':case 'manufacturer':case 'customer':case 'stage':
            query_order = `ORDER BY \`lots\`.\`${req.query.orderActive}\``;
        break;
        }
    }

    query_order = query_order.concat(` ${direction}`);

    if (filter_stage || filter_region || filter_manufacturer) {
        if (filter_stage) {
            const stages = ['new', 'winner', 'sent_kp', 'call', 'apply', 'contract', 'payment', 'placing_orders', 'delivered', 'failed', 'finish'];
            if (stages.indexOf(filter_stage) === -1) {
                res.sendStatus(400);
                return;
            }

            query_stage = '`lots`.`stage`=?';
            params.push(filter_stage);
        }

        if (filter_region) {
            const regions = ['home_prim', 'home_dv', 'home_dvsib', 'dv', 'sib', 'west'];
            if (regions.indexOf(filter_region) === -1) {
                res.sendStatus(400);
                return;
            }

            query_stage = '`lots`.`region`=? AND '.concat(query_stage);
            params = [filter_region].concat(params);
        }

        if (filter_manufacturer) {
            query_stage = '`lots`.`manufacturer`=? AND '.concat(query_stage);
            params = [filter_manufacturer].concat(params);
        }
    }

    Promise.all([
        new Promise((resolve, reject) => {
            // всего записей
            const query_sql = 'SELECT count(`lots`.`id`) AS `total` FROM `lots` WHERE '.concat(query_stage);

            conn.query(query_sql, params, (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
        
                resolve(result[0]);
            });
        }),
        new Promise((resolve, reject) => {
            const query_sql = 'SELECT `lots`.*, SUM(`kp_positions`.`cnt`*`kp_positions`.`price`) AS `sum` '+
                'FROM `lots` ' +
                'LEFT JOIN `kp_positions` ON `kp_positions`.`lot_id`=`lots`.`id` WHERE '.concat(query_stage, ' GROUP BY `lots`.`id` ', query_order, ' LIMIT ?, ?');

            conn.query(query_sql, params.concat([pageIndex*count, count]), (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
        
                resolve(result);
            });
        }),
        new Promise((resolve, reject) => {
            const query_sql = 'SELECT `lots`.`id`, count(distinct(`files`.`id`)) AS `files_cnt` '+
                'FROM `lots` ' +
                'LEFT JOIN `files` ON `files`.`lot_id`=`lots`.`id` WHERE '.concat(query_stage, ' GROUP BY `lots`.`id` ', query_order, ' LIMIT ?, ?');
            
            conn.query(query_sql, params.concat([pageIndex*count, count]), (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
        
                resolve(result);
            });
        })
    ]).then((data) => {
        const total = data[0].total|0;
        const records = data[1]||[];
        const files_counts = data[2]||[];

        let result = {
            total : total,
            items : [],
        };

        let files_cnt = new Map();
        for (let i = 0; i < files_counts.length; i++) {
            files_cnt.set(files_counts[i].id, files_counts[i].files_cnt);
        }

        let item;
        for (let i = 0; i < records.length; i++) {
            item = Object.assign(records[i], {files_cnt : files_cnt.get(records[i].id)});
            result.items.push(item);
        }

        res.send(result);
    }).catch((err) => {
        console.error(err);
        res.sendStatus(500);
    });
});

app.get('/api/lots/files', function (req, res) {
    conn.query("SELECT `id`,`name` FROM `files` WHERE `lot_id`=?", [req.query.lot_id], (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        res.send(result);
    });
});

/**
 * создание лота
 */
app.post('/api/lots', function (req, res) {
    const query_sql = 'INSERT INTO `lots` SET ?';
    let query_data = {
        number : req.body.number,
        name : req.body.name,
        manufacturer : req.body.manufacturer,
        customer : req.body.customer,
        region : req.body.region,
        url : req.body.url,
        date_end_auctions : req.body.date_end_auctions,
        date_review : req.body.date_review,
        date_stage_change : new Date(),
        stage : req.body.stage,
     };

    conn.query(query_sql, query_data, (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        query_data.id = result.insertId;

        global.app.get('io').emit('create_lot', query_data);

        res.status(201).json(query_data);
    });
});

/**
 * редактирование
 */
app.put('/api/lots', function (req, res) {
    if (!req.body.id) {
        res.sendStatus(404);
        return;
    }

    const prom = new Promise((resolve, reject) => {
        const query_sql = 'SELECT `id`,`date_stage_change`,`stage` FROM `lots` WHERE `id`=?';
        conn.query(query_sql, [req.body.id], (err, result) => {
            if (err != null) {
                reject(err);
                return;
            }

            if (!result[0].id) {
                reject(new Error('not found'));
                return;
            }

            resolve(result[0]);
        });
    });

    prom.then((val) => {
        let date_stage_change = val.date_stage_change;
        if (val.stage != req.body.stage) {
            date_stage_change = new Date();
        }


        const query_sql = 'UPDATE `lots` SET `number`=?,`name`=?,`manufacturer`=?,`customer`=?,`winner`=?,`contacts`=?,`region`=?,`date_end_auctions`=?,`date_review`=?,`favorite`=?,`url`=?,`date_stage_change`=?,`comment`=?,`stage`=? WHERE `id`=?';
        var query_data = [
            req.body.number,
            req.body.name,
            req.body.manufacturer,
            req.body.customer,
            req.body.winner,
            req.body.contacts,
            req.body.region,
            req.body.date_end_auctions,
            req.body.date_review,
            req.body.favorite == 'Y' ? 'Y' : 'N',
            req.body.url,
            date_stage_change,
            req.body.comment,
            req.body.stage,
            req.body.id,
        ];
    
        conn.query(query_sql, query_data, (err, result) => {
            if (err !== null) {
                console.error(err);
                res.sendStatus(500);
                return;
            }
    
            const responce = {
                id : req.body.id,
                number : req.body.number,
                name : req.body.name,
                manufacturer : req.body.manufacturer,
                customer : req.body.customer,
                winner : req.body.winner,
                contacts : req.body.contacts,
                region : req.body.region,
                date_end_auctions : req.body.date_end_auctions,
                date_review : req.body.date_review,
                favorite : req.body.favorite == 'Y' ? 'Y' : 'N',
                url : req.body.url,
                date_stage_change : date_stage_change,
                comment : req.body.comment,
                stage : req.body.stage,
            };
    
            global.app.get('io').emit('update_lot', responce);
    
            res.status(202).json(responce);
        });
    });
});

app.delete('/api/lots', function (req, res) {
    const id = req.query.id|0;

    if (!id) {
        res.sendStatus(404);
        return;
    }

    const query_data = [ id ];

    const query_sql = "SELECT `stage` FROM `lots` WHERE `id`=?";
    conn.query(query_sql, query_data, (err, result) => {
        if (err !== null) {
            console.error(err);
            res.sendStatus(500);
            return;
        }

        if (!Array.isArray(result) || result.length == 0) {
            res.sendStatus(404);
            return;
        }

        if (result[0].stage === 'failed') {
            // полное удаление
            // удаление файлов
            const pRmFiles = new Promise((resolve, reject) => {
                const query_sql = "SELECT `dist_name` FROM `files` WHERE `lot_id`=?";
                conn.query(query_sql, query_data, (err, result) => {
                    try {
                        if (err !== null) {
                            throw err;
                        }

                        if (Array.isArray(result)) {
                            for (let i = 0; i < result.length; i++) {
                                fs.unlinkSync(`./uploads/${result[i].dist_name}`);
                            }
                        }
    
                        resolve();
                    } catch(err) {
                        reject(err);
                    }
                });
            });

            pRmFiles.then(() => {
                const query_sql = "DELETE FROM `lots` WHERE `id`=?";
                conn.query(query_sql, query_data, (err) => {
                    if (err !== null) {
                        console.error(err);
                        res.sendStatus(500);
                        return;
                    }
    
                    global.app.get('io').emit('delete_lot', id);
                    res.sendStatus(204);
                });
            });
        } else {
            // в архив
            const query_sql = "UPDATE `lots` SET `stage`='failed' WHERE `id`=?";
            conn.query(query_sql, query_data, (err) => {
                if (err !== null) {
                    console.error(err);
                    res.sendStatus(500);
                    return;
                }

                global.app.get('io').emit('delete_lot', id);
                res.sendStatus(204);
            });
        }
    });
});
/**
 * скачать КП
 */
app.get('/api/lot/downloadkp/', function (req, res) {
    const lot_id = req.query.lot_id|0;
    if (lot_id === 0) {
        res.sendStatus(404);
        return;
    }

    const NDS = 0.18;
    const FIRST_ROW_NUM = 8;

    const conn = require('../db');
    const Excel = require('exceljs');
    const NumFMT = '# ###.00';

    const workbook = new Excel.Workbook();
    const sheet = workbook.addWorksheet('Sheet', {
        pageSetup: {
            paperSize: 9,
            orientation:'portrait',
            scale : 78,
            fitToPage: true,
            fitToHeight: 1,
            fitToWidth: 1,
            margins : {
                left: 0.3, right: 0.3,
                top: 0.1, bottom: 0.30,
                header: 0.2, footer: 0.2
            }
        },
        properties : {
            defaultRowHeight : 15
        }
    });

    // первая колонка
    for (let i = 1; i < 9; i++) {
        let dobCol = sheet.getColumn(i);
    
        switch (i) {
        case 1:
            dobCol.width = 2.5; // 2
        break;
        case 2: case 4: case 5:
            dobCol.width = 7.0; // 6.1
        break;
        case 3:
            dobCol.width = 68; // 44
        break;
        case 6:
            dobCol.width = 8; // 12
        break;
        case 7: case 8:
            dobCol.width = 12.27; // 10
        break;
        }
    }

    const style_alignment = {vertical: 'middle', horizontal: 'center', wrapText: true};
    const style_border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    };
    const style_font = {size : 14, name : 'Times New Roman', wrapText : true};
    const style_font_small = {size : 12, name : 'Times New Roman', wrapText : true};
    const style_font_bold = { name: 'Times New Roman', wrapText : true, family: 4, size: 12, bold: true };
    
    let row;
    for (let i = 0; i < 8; i++) {
        row = sheet.getRow(1 + i);
        row.height = 15;
    }

    let row_num = 15;

    // город, дата
    row = sheet.getRow(FIRST_ROW_NUM + 5);
    cell = row.getCell(2);
    cell.font = style_font_small;
    cell.value = 'г. Владивосток';

    const months = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
    
    // таблица
    row = sheet.getRow(FIRST_ROW_NUM + 7);
    row.height = 50;
    
    for (let i = 2; i < 9; i++) {
        cell = row.getCell(i);
        
        cell.border = style_border;
        cell.alignment = style_alignment;
        cell.font = style_font_small;
    
        switch (i) {
        case 2:
            cell.value = '№';
        break;
        case 3:
            cell.value = 'Наименование';
        break;
        case 4:
            cell.value = 'Кол-во ед.';
        break;
        case 5:
            cell.value = 'Ед. изм.';
        break;
        case 6:
            cell.value = 'Цена, руб с НДС/ед.';
        break;
        case 7:
            cell.value = 'Сумма, руб, с НДС';
        break;
        case 8:
            cell.value = 'Срок поставки';
        break;
        }
    }
    
    // получим КП
    const prom = new Promise((resolve, reject) => {
        const query_sql = "SELECT `id`,`kp_name`,`kp_delivery_time`,`kp_delivery_term`,`kp_payment_term`,`date_actual` FROM `lots` WHERE `id`=?";

        conn.query(query_sql, [lot_id], (err, result) => {
            if (err !== null) {
                console.error(err);
                reject(err);
                return;
            }

            if (result.length === 0) {
                reject(404);
                return;
            }

            const kp = result[0];

            const query_sql = "SELECT `name`,`cnt`,`unit`,`price`,`delivery` FROM `kp_positions` WHERE `lot_id`=?";
            conn.query(query_sql, [lot_id], (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
    
                const responce = {
                    kp : kp,
                    positions : result||[],
                };
    
                resolve(responce);
            });
        });
    });

    prom.then((data) => {
        // дата
        row = sheet.getRow(FIRST_ROW_NUM + 5);

        let cell = row.getCell(8);
        cell.font = style_font_small;
        cell.alignment = {horizontal : 'right'};

        const d = data.kp.date_actual ? new Date(Date.parse(`${data.kp.date_actual}10`)) : new Date();
        cell.value = `«${d.getDate()}» ${months[d.getMonth()]} ${d.getFullYear()}`;

        // для
        row = sheet.getRow(FIRST_ROW_NUM);
        cell = row.getCell(8);
        cell.value = `Для ${data.kp.kp_name}`;
        cell.alignment = {horizontal: 'right'};
        cell.font = { name: 'Times New Roman', family: 4, size: 14, underline: true };

        row = sheet.getRow(FIRST_ROW_NUM + 2);
        cell = row.getCell(1);
        cell.value = `КОММЕРЧЕСКОЕ ПРЕДЛОЖЕНИЕ АУ №${data.kp.id}`;
        cell.font = { name: 'Times New Roman', family: 4, size: 16, bold: true };
        cell.alignment = style_alignment;
        sheet.mergeCells(`A${FIRST_ROW_NUM+2}:H${FIRST_ROW_NUM+2}`);

        let row_num = FIRST_ROW_NUM + 7;

        for (let i = 0; i < data.positions.length; i++) {
            row_num++
        
            row = sheet.getRow(row_num);
            // порядковый номер
            cell = row.getCell(2);
            cell.border = style_border;
            cell.alignment = style_alignment;
            cell.font = style_font_small;
            cell.value = i + 1;
            // наименование
            cell = row.getCell(3);
            cell.border = style_border;
            cell.font = style_font_small;
            cell.alignment = {wrapText: true};
            cell.value = data.positions[i].name;
            // количество
            cell = row.getCell(4);
            cell.border = style_border;
            cell.alignment = style_alignment;
            cell.font = style_font_small;
            cell.value = data.positions[i].cnt;
            // еденица измерения
            cell = row.getCell(5);
            cell.border = style_border;
            cell.alignment = style_alignment;
            cell.font = style_font_small;
            cell.value = data.positions[i].unit;
            // цена
            cell = row.getCell(6);
            cell.border = style_border;
            cell.alignment = {horizontal : 'right'};
            cell.font = style_font_small;
            cell.numFmt = NumFMT;
            cell.value = data.positions[i].price;
            // сумма
            cell = row.getCell(7);
            cell.border = style_border;
            cell.alignment = {horizontal : 'right'};
            cell.font = style_font_bold;
            cell.numFmt = NumFMT;
            cell.value = { formula: `F${row_num}*D${row_num}` };

            // срок поставки
            cell = row.getCell(8);
            cell.border = style_border;
            cell.alignment = style_alignment;
            cell.font = style_font_small;
            cell.value = data.positions[i].delivery;
        }

        // итого
        row_num += 2;
        sheet.mergeCells(`E${row_num}:F${row_num}`);
        row = sheet.getRow(row_num);
        
        cell = row.getCell(5);
        cell.border = style_border;
        cell.alignment = {vertical: 'middle', horizontal: 'right', wrapText: true};
        cell.font = style_font_bold;
        cell.value = 'Итого, руб с НДС';
        
        cell = row.getCell(7);
        cell.alignment = {horizontal : 'right'};
        cell.border = style_border;
        cell.font = style_font_bold;
        cell.numFmt = NumFMT;
        cell.value = { formula: `=SUM(G8:G${row_num-2})` };
        
        row_num++;
        row = sheet.getRow(row_num);
        cell = row.getCell(6);
        cell.font = style_font_bold;
        cell.alignment = {horizontal: 'right'};
        cell.value = 'В т.ч. НДС';

        cell = row.getCell(7);
        cell.font = style_font_bold;
        cell.numFmt = NumFMT;
        cell.alignment = {horizontal : 'right'};
        cell.value = { formula: `=G${row_num-1}/${1+NDS}*${NDS}` };

        // доп информация
        cell = row.getCell(3);
        cell.font = style_font_small;
        cell.value = `Срок поставки: ${data.kp.kp_delivery_time||''}`;
        
        row_num++;
        row = sheet.getRow(row_num);
        cell = row.getCell(3);
        cell.font = style_font_small;
        cell.value = `Условия поставки: ${data.kp.kp_delivery_term||''}`;
        
        row_num++;
        row = sheet.getRow(row_num);
        cell = row.getCell(3);
        cell.font = style_font_small;
        cell.value = `Условия оплаты: ${data.kp.kp_payment_term||''}`;
        
        // примечание
        row_num += 3;
        row = sheet.getRow(row_num);
        cell = row.getCell(3);
        cell.font = { color : {argb: 'FF000000'}, size : 10, name : 'Times New Roman' };
        cell.value = 'Цены действительны на всю спецификацию, при изменении спецификации цены могут быть пересмотрены';
        
        row_num++;
        row = sheet.getRow(row_num);
        cell = row.getCell(3);
        cell.style = {
            font :  { color : {argb: 'FF000000'}, size : 10, name : 'Times New Roman' },
        };
        cell.value = 'Настоящее предложение является предварительным и не является основанием для оплаты.';
        
        // подпись
        row_num += 12;
        row = sheet.getRow(row_num);
        cell = row.getCell(3);
        cell.font = style_font;
        cell.value = '      С уважением, директор';
        
        row_num++;;
        row = sheet.getRow(row_num);
        cell = row.getCell(3);
        cell.font = style_font;
        cell.value = 'ООО «Лидер Электро Прибор»';

        cell = row.getCell(7);
        cell.font = style_font;
        cell.value = '              Е. А. Быков';
        
        // печать и роспись
        const imageHeader = workbook.addImage({
            filename: './templates_xlsx/header.jpg',
            extension: 'jpg',
        });
        const imageStamp = workbook.addImage({
            filename: './templates_xlsx/stamp.png',
            extension: 'png',
        });
        const imageSign = workbook.addImage({
            filename: './templates_xlsx/sign.png',
            extension: 'png',
        });

        row_num -= 2;

        for (let i = 0; i < 12; i++) {
            row = sheet.getRow(row_num + i);
            row.height = 15;
        }
        
        sheet.addImage(imageHeader, {
            tl: { col: 1, row: 0 }, // x1, y1   верх лево
            br: { col: 8, row: 5 }, // x2, y2   низ право
            editAs: 'absolute',
        });
        
        sheet.addImage(imageStamp, {
            tl: { col: 4.5, row: row_num-0.5 }, // x1, y1   верх лево
            br: { col: 7.05, row: row_num + 7.99 }, // x2, y2   низ право
            editAs: undefined,
        });
        sheet.addImage(imageSign, {
            tl: { col: 3.0, row: row_num }, // x1, y1   верх лево
            br: { col: 5.99, row: row_num + 2.5 }, // x2, y2   низ право
            editAs: 'oneCell'
        });
        
        // область печати страницы
        sheet.pageSetup.printArea = `$A$1:$H$${row_num+10}`;

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", `attachment; filename=kp_${data.kp.id}.xlsx`);
        workbook.xlsx.write(res).then(() => {
            res.end();
        });
    });

    prom.catch((err) => {
        console.error(err);
        res.sendStatus(500);
    });
});


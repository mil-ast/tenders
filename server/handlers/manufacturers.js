const conn = require('../db');

app.get('/api/manufacturers', function (req, res) {
    const query_sql = 'SHOW COLUMNS FROM `lots` WHERE field=?';
    conn.query(query_sql, ['manufacturer'], (err, result) => {
        if (err !== null) {
            reject(err);
            return;
        }

        let types = result[0].Type.substr(5, result[0].Type.length - 6);
        types = types.replace(/'/g, '');

        res.send(types.split(','));
    });
});

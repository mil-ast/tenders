import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ManufacturersService {
    private url: string = '/api/manufacturers';
    private list: any[] = [];

	constructor(
		private http: HttpClient
    ) { }
    
    Sync() {
        const sbj: Subject<string[]> = new Subject();

        this.http.get(this.url).subscribe((res: string[]) => {
            this.list = res || [];
            sbj.next(this.list);
        }, (err: Error) => {
            console.error(err);
            sbj.error(err);
        });

        return sbj;
    }

    Get(): string[] {
        return this.list;
    }
}

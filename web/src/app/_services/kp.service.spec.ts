import { TestBed, inject } from '@angular/core/testing';

import { KpService } from './kp.service';

describe('KpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KpService]
    });
  });

  it('should be created', inject([KpService], (service: KpService) => {
    expect(service).toBeTruthy();
  }));
});

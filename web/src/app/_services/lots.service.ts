import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LotsService {
    private url = '/api/lots';

	constructor(
		private http: HttpClient
	) { }

	Get(pageIndex: number = 0, count: number = 25, filter: any, order: any) {
		const region: string = filter.region ? filter.region : null;
		const stage: string = filter.stage ? filter.stage : null;
		const manufacturer: string = filter.manufacturer ? filter.manufacturer : null;

		const order_active: string = order.active ? order.active : 'id';
		const order_direction: string = order.direction ? order.direction : 'desc';

		let url = this.url.concat(`?pageIndex=${pageIndex}&count=${count}`);
		if (region) {
			url = url.concat(`&region=${region}`);
		}
		if (stage) {
			url = url.concat(`&stage=${stage}`);
		}
		if (manufacturer) {
			url = url.concat(`&manufacturer=${manufacturer}`);
		}

		url = url.concat(`&orderActive=${order_active}&orderDirection=${order_direction}`);

		return this.http.get(url);
	}

	GetFiles(lot_id: number) {
		return this.http.get(this.url.concat(`/files?lot_id=${lot_id}`));
	}

	DeleteFile(id: number) {
		return this.http.delete(`/api/files/${id}`);
	}

	Create(data: any) {
		return this.http.post(this.url, data);
	}

	Update(data: any) {
		return this.http.put(this.url, data);
	}

	FileUpload(data: FormData) {
		return this.http.post(this.url.concat('/upload'), data);
	}

	Delete(id: number) {
		return this.http.delete(this.url.concat('?id=', id.toString()));
	}
}
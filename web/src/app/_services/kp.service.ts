import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class KpService {
    private url = '/api/kp';
	constructor(
		private http: HttpClient
	) { }

	Get(lot_id: number) {
		return this.http.get(`${this.url}/?lot_id=${lot_id}`);
	}
	Update(data: any) {
		return this.http.put(this.url, data);
	}

	GetPositions(lot_id: number) {
		return this.http.get(`${this.url}/positions/?lot_id=${lot_id}`);
	}

	CreatePosition(data) {
		return this.http.post(this.url.concat('/positions'), data);
	}

	UpdatePosition(data) {
		return this.http.put(this.url.concat('/positions'), data);
	}

	DeletePosition(id: number) {
		return this.http.delete(`${this.url}/positions/?id=${id}`);
	}
}

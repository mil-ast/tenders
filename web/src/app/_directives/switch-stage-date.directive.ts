import { Directive, Input, OnInit, ElementRef, HostListener } from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[appSwitchStageDate]'
})
export class SwitchStageDateDirective implements OnInit {
    @Input() date: string;
    @Input() stage: string;

    @HostListener('mouseenter') onMouseEnter() {
        this.render(false);
    }
    @HostListener('mouseleave') onMouseLeave() {
        this.render(true);
    }

    private el_span;

    constructor(
        private el: ElementRef,
    ) {}

    ngOnInit() {
        this.el_span = this.el.nativeElement.children[0];
        this.render(true);
    }

    private render(time_ago: boolean) {
        if (!this.stage || this.stage === 'new') {
            this.el_span.textContent = '';
            return;
        }
        this.el_span.textContent = this.formatDateChangeStage(time_ago);
    }

    private formatDateChangeStage(time_ago: boolean): string {
        const date = moment(this.date);
        if (!date.isValid()) {
            return '';
        }

        if (time_ago) {
            return date.fromNow(false);
        } else {
            return date.format('DD MMMM YYYY');
        }        
    }
}
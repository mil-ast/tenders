import * as moment from 'moment';

export interface Element {
    id?: number;
    name: string;
    winner?: string;
    contacts?: string;
    customer?: string;
    manufacturer: string;
    number: string;
    kp_stage?: string;
    region: string;
    stage?: string;
    sum?: number;
    files_cnt?: number;
    date_end_auctions?: string;
    date_review?: string;
    favorite?: string;
    url?: string;
    date_stage_change?: string;
    comment?: string;
    // КП
    kp_name?: string;
    kp_delivery_time?: string;
    kp_delivery_term?: string;
    kp_payment_term?: string;
}
export interface ElementApi {
    total: number;
    items: Element[];
}

export class Lot {
    id: number;
    name: string;
    winner: string;
    contacts: string;
    customer: string;
    manufacturer: string;
    number: string;
    kp_stage: string;
    region: string;
    stage: string;
    sum: number;
    files_cnt: number;
    date_end_auctions: string;
    date_review: string;
    favorite: string;
    url: string;
    date_stage_change: string;
    comment: string;
    // КП
    kp_name: string;
    kp_delivery_time: string;
    kp_delivery_term: string;
    kp_payment_term: string;

    constructor(data: Element) {
        this.id = data.id|0;
        this.sum = data.sum|0;
        this.files_cnt = data.files_cnt|0;
        
        this.UpdateLot(data);
        this.UpdateKP(data);
    }

    KpSuccess(): boolean {
        return this.sum > 0 && this.kp_name != null && this.kp_delivery_time != null && this.kp_delivery_term != null && this.kp_payment_term != null;
    }

    UpdateLot(data: Element) {
        this.name = data.name||null;
        this.winner = data.winner||null;
        this.contacts = data.contacts||null;
        this.customer = data.customer||null;
        this.manufacturer = data.manufacturer||null;
        this.number = data.number||null;
        this.date_end_auctions = data.date_end_auctions||null;
        this.date_review = data.date_review||null;
        this.favorite = data.favorite||'N';
        this.url = data.url||null;
        this.date_stage_change = data.date_stage_change||null;
        this.comment = data.comment||null;

        switch(data.region) {
        case 'home_dv': case 'home_dvsib': case 'dv': case 'sib': case 'west':
            this.region = data.region;
        break;
        default:
            this.region = 'home_prim';
        }

        //ENUM()
        switch(data.stage) {
        case 'new':
        case 'winner':
        case 'sent_kp':
        case 'sent_kp2':
        case 'call':
        case 'apply':
        case 'contract':
        case 'payment':
        case 'placing_orders':
        case 'delivered':
        case 'failed':
        case 'finish':
            this.stage = data.stage;
        break;
        default:
            this.stage = null;
        }
    }

    IsDateActual(): boolean {
        let date = null;

        if (this.date_review != null) {
            date = this.date_review;
        } else if (this.date_end_auctions != null) {
            date = this.date_end_auctions;
        } else {
            return false;
        }

        const m_date = moment(date);
        if (!m_date.isValid()) {
            return false;
        }

        return moment().isAfter(m_date);
    }

    UpdateKP(data: Element) {
        // КП
        this.kp_name = data.kp_name||null;
        this.kp_delivery_time = data.kp_delivery_time||null;
        this.kp_delivery_term = data.kp_delivery_term||null;
        this.kp_payment_term = data.kp_payment_term||null;
    }

    Winner(): boolean {
        switch (this.stage) {
        case 'winner':
        case 'sent_kp':
        case 'sent_kp2':
        case 'call':
        case 'apply':
        case 'contract':
        case 'payment':
        case 'placing_orders':
        case 'delivered':
        case 'failed':
        case 'finish':
            return this.contacts !== null;
        }
        return false;
    }

    FormatDateChangeStage() {
        if (this.date_stage_change == null) {
            return '';
        }

        return moment(this.date_stage_change).toNow(true);
    }
}
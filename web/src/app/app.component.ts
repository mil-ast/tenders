import { Component, OnInit } from '@angular/core';
import { MatDialog, Sort, PageEvent } from '@angular/material';
import { LotsService } from './_services/lots.service';
import { Lot, Element, ElementApi } from './_classes/lot';
import { DialogCreateLotComponent } from './dialogs/create-lot/create-lot.component';
import * as io from 'socket.io-client';
import { ManufacturersService } from './_services/manufacturers.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
    list: Lot[] = [];
    manufactures: string[] = [];
    private indexes: Map<number, Lot> = new Map();

    total: number = 0;
    pageIndex: number = 0;
    pageSize: number = 20;
    pageSizeOptions: number[] = [10, 15, 20, 25, 30, 50, 100];
    order: Sort = {
        active : 'date',
        direction : 'asc',
    };

    ws_ok: boolean = false;
    is_sync: boolean = true;
    filter_region: string = null;
    filter_manufacturer: string = null;
    filter_stage: string = null;

    constructor(
        public dialog: MatDialog,
        private serviceLots: LotsService,
        private serviceManufacturers: ManufacturersService,
    ) {
        const storPageSize: string = localStorage.getItem('pageSize');
        if (storPageSize) {
            this.pageSize = parseInt(storPageSize);
        }
    }

    changePaginator(event) {
        this.pageIndex = event.pageIndex;
        this.pageSize = event.pageSize;

        localStorage.setItem('pageSize', this.pageSize.toString());

        this.getData();
    }

    ngOnInit() {
        const req = this.serviceManufacturers.Sync();
        req.subscribe((res: string[]) => {
            this.manufactures = res||[];
        });

        this.getData();

        const socket = io();
        socket.on('connect', () => {
            this.ws_ok = true;
        });
        socket.on('connect_error', () => {
            this.ws_ok = false;
        });
        // добавление лота
        socket.on('create_lot', (msg: Element) => {
            const lot = new Lot(msg);
            this.indexes.set(lot.id, lot);
            this.list.unshift(lot);
        });
        // изменение лота
        socket.on('update_lot', (msg: Element) => {
            const lot = this.indexes.get(msg.id);
            if (lot) {
                lot.UpdateLot(msg);
            }
        });
        // удаление лота
        socket.on('delete_lot', (msg: number) => {
            const lot = this.indexes.get(msg);
            if (lot) {
                this.indexes.delete(lot.id);

                const index = this.list.indexOf(lot);
                if (index !== -1) {
                    this.list.splice(index, 1);
                }
            }
        });
    }

    private getData() {
        this.is_sync = true;
        this.indexes.clear();
        this.list = [];

        const filter: any = {
            region : this.filter_region,
            stage : this.filter_stage,
            manufacturer : this.filter_manufacturer,
        };

        const order: any = {
            active : this.order.active,
            direction : this.order.direction,
        };
        
        const req = this.serviceLots.Get(this.pageIndex, this.pageSize, filter, order);
        req.subscribe((res: ElementApi) => {
            if (!Array.isArray(res.items)) {
                return;
            }

            this.total = res.total;

            for (let i = 0; i < res.items.length; i++) {
                const lot = new Lot(res.items[i]);

                this.indexes.set(lot.id, lot);
                this.list.push(lot);
            }
        }, (err) => {
            console.log(err);
        }, () => {
            this.is_sync = false;
        });
    }

    ShowDialogCreateLot() {
        let dialogRef = this.dialog.open(DialogCreateLotComponent, {
            width: '700px',
            position: {
                top: '20px',
                left: '20px',
            },
            data: {}
        });

        dialogRef.afterClosed().subscribe((result: Lot) => {
            if (!result) {
                return;
            }
        });
    }

    ChangeFilter() {
        this.getData();
    }

    ResetFilter() {
        this.filter_region = null;
        this.filter_stage = null;

        this.getData();
        return false;
    }

    SortData(sort: Sort) {
        if (sort.direction === '') {
            this.order = {
                direction : 'desc',
                active : 'id'
            };
        } else {
            this.order = sort;
        }
        
        this.getData();
    }
}
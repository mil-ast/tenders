import { Pipe, PipeTransform } from '@angular/core';
import { Lot } from '../_classes/lot';

@Pipe({
    name: 'filter',
    pure: false
})

export class FilterPipe implements PipeTransform {
    transform(array: Lot[], region: string, stage: string) {
        if (!Array.isArray(array)) {
            return [];
        }

		if (region === null && stage === null) {
			return array;
        }

        return array.filter((item: Lot) => {
            if (region !== null) {
                if (item.region !== region) {
                    return false;
                }
            }

            if (stage !== null) {
                if (item.stage !== stage) {
                    return false;
                }
            }

            return true;
        });
    }
}
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LotsService } from '../_services/lots.service';
import { Lot, Element } from '../_classes/lot';
import { DialogUpdateLotComponent } from '../dialogs/update-lot/update-lot.component';
import { DialogKPComponent } from '../dialogs/kp/kp.component';

@Component({
  selector: 'app-lot-description',
  templateUrl: './lot-description.component.html',
  styleUrls: ['./lot-description.component.less']
})
export class LotDescriptionComponent implements OnInit {
    @Input() model: Lot;

    confirm: boolean = false;
    files: string[] = [];
  
    constructor(
        public dialog: MatDialog,
        private serviceLots: LotsService
    ) { }

    ngOnInit() {
        const req = this.serviceLots.GetFiles(this.model.id);
        req.subscribe((res: any[]) => {
            if (!Array.isArray(res)) {
                return;
            }

            this.files = res;
        });
    }

    ShowFormUpdate() {
        let dialogRef = this.dialog.open(DialogUpdateLotComponent, {
            width: '700px',
            position: {
                top : '40px',
                left:'40px'
            },
            data: this.model
        });

        dialogRef.afterClosed().subscribe((result: Element) => { });
        return false;
    }

    ShowFormKP() {
        let dialogRef = this.dialog.open(DialogKPComponent, {
            width: '900px',
            position: {
                top : '40px',
                left:'60px'
            },
            data: this.model
        });

        dialogRef.afterClosed().subscribe((result: Element) => {
            if (!result) {
                return;
            }

        });
        return false;
    }

	SubmitFileUpload(event) {
		let fileList: FileList = event.target.files;
		
		if (fileList.length === 0) {
			return;
        }

        let formData = new FormData();
        formData.append('lot_id', this.model.id.toString());

        for (let i = 0; i < fileList.length; i++) {
            formData.append('files[]', fileList[i], fileList[i].name);
        }

        const req = this.serviceLots.FileUpload(formData);
        req.subscribe((res: any) => {
            if (!Array.isArray(res)) {
                return;
            }

            this.files = this.files.concat(res);
            this.model.files_cnt = this.files.length;
        }, (err) => {
            console.log(err);
        });
    }

    Confirm(event) {
        this.confirm = event;
        return false;
    }

    ClickDeleteFile(file: any) {
        if (!confirm('Удалить файл?')) {
            return false;
        }

        const req = this.serviceLots.DeleteFile(file.id);
        req.subscribe(() => {
            const index = this.files.indexOf(file);
            if (index !== -1) {
                this.files.splice(index, 1);
            }
        }, (err) => {
            console.error(err);
        });

        return false;
    }

    Delete() {
        const req = this.serviceLots.Delete(this.model.id);
        req.subscribe(() => {
            
        }, (err) => {
            console.error(err);
        });
        return false;
    }
}

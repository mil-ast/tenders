import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotDescriptionComponent } from './lot-description.component';

describe('LotDescriptionComponent', () => {
  let component: LotDescriptionComponent;
  let fixture: ComponentFixture<LotDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

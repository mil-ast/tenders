import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCreateLotComponent } from './create-lot.component';

describe('DialogCreateLotComponent', () => {
  let component: DialogCreateLotComponent;
  let fixture: ComponentFixture<DialogCreateLotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCreateLotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCreateLotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

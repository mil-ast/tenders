import {Component, Inject} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LotsService } from '../../_services/lots.service';
import { ManufacturersService } from '../../_services/manufacturers.service';
import { Lot, Element } from '../../_classes/lot'; 

@Component({
    selector: 'dialog-create-lot',
    templateUrl: './create-lot.component.html',
    styleUrls: ['./create-lot.component.less']
})
export class DialogCreateLotComponent {
    reactiveForm: FormGroup;
    manufacturers: string[] = [];

    constructor(
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<DialogCreateLotComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private serviceLots: LotsService,
        private serviceManufacturers: ManufacturersService,
    ) {
		this.reactiveForm = this.fb.group({
			name: ['', [Validators.required, Validators.maxLength(65000)]],
			customer: [null, Validators.maxLength(65000)],
			manufacturer: ['', Validators.required],
			number: ['', Validators.required],
            region: 'home_prim',
            date_end_auctions : null,
            date_review : null,
			url: [null, [Validators.pattern(/^(http|https):\/\/[^ "]+$/), Validators.maxLength(255)]],
			stage: null, 
        });

        this.manufacturers = this.serviceManufacturers.Get();
    }

    Submit() {
		let date_end_auctions = null;
		if (this.reactiveForm.value.date_end_auctions) {
			date_end_auctions = this.reactiveForm.value.date_end_auctions.format('YYYY-MM-DD');
		}
		let date_review = null;
		if (this.reactiveForm.value.date_review) {
			date_review = this.reactiveForm.value.date_review.format('YYYY-MM-DD');
		}

        const values = {
            name : this.reactiveForm.value.name,
            customer : this.reactiveForm.value.customer,
            manufacturer : this.reactiveForm.value.manufacturer,
            number : this.reactiveForm.value.number,
            region : this.reactiveForm.value.region,
            date_end_auctions : date_end_auctions,
            date_review : date_review,
            url : this.reactiveForm.value.url,
            stage : this.reactiveForm.value.stage,
        };

        const req = this.serviceLots.Create(values);
        req.subscribe((res: Element) => {
            const lot: Lot = new Lot(res);
            this.dialogRef.close(lot)
        }, (err) => {
            console.error(err);
        });
        
        return false;
    }

    onNoClick(): void {
        this.dialogRef.close(null);
    }
}
import {Component, Inject, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { KpService } from '../../_services/kp.service'; 
import { Lot } from '../../_classes/lot'; 

@Component({
  selector: 'app-kp',
  templateUrl: './kp.component.html',
  styleUrls: ['./kp.component.less']
})
export class DialogKPComponent implements OnInit {
    reactiveForm: FormGroup;
    reactivePosition: FormGroup;
    reactivePositionUpdate: FormGroup;

    positions: any[] = [];
    editPosition: any = null;

    constructor(
        private serviceKp: KpService,
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<DialogKPComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Lot
    ) {
        const name = data.kp_name ? data.kp_name : (data.winner||'').substr(0,255);
        this.reactiveForm = this.fb.group({
            kp_name: [name, Validators.maxLength(255)],
            kp_delivery_time: [data.kp_delivery_time, Validators.maxLength(255)],
            kp_delivery_term: [data.kp_delivery_term, Validators.maxLength(255)],
            kp_payment_term: [data.kp_payment_term, Validators.maxLength(255)],
        });

        this.reactivePosition = this.fb.group({
            name: [null, [Validators.required, Validators.maxLength(65000)]],
            cnt: [1, [Validators.required, Validators.min(0)]],
            unit: ['шт', Validators.required],
            price: [null, [Validators.required, Validators.min(0.0)]],
            delivery : null,
        });

        this.reactivePositionUpdate = this.fb.group({
            id: null,
            name: [null, [Validators.required, Validators.maxLength(65000)]],
            cnt: [1, [Validators.required, Validators.min(0)]],
            unit: ['шт', Validators.required],
            price: [null, [Validators.required, Validators.min(0.0)]],
            delivery : null,
        });
    }

    ngOnInit() {
        // получим все позиции
        const req = this.serviceKp.GetPositions(this.data.id);
        req.subscribe((res: any[]) => {
            if (!Array.isArray(res)) {
                return;
            }
            this.positions = res||[];
        });
    }

    Submit() {
        if (this.reactiveForm.invalid) {
            return false;
        }

        const data = {
            lot_id : this.data.id,
            kp_name : this.reactiveForm.value.kp_name,
            kp_delivery_time : this.reactiveForm.value.kp_delivery_time,
            kp_delivery_term : this.reactiveForm.value.kp_delivery_term,
            kp_payment_term : this.reactiveForm.value.kp_payment_term,
        };

        const req = this.serviceKp.Update(data);
        req.subscribe((res: any) => {
            this.data.UpdateKP(res);
            this.dialogRef.close(res)
        });

        return false;
    }

    SubmitCreatePosition() {
        if (this.reactivePosition.invalid) {
            return false;
        }

        const data = {
            lot_id : this.data.id,
            name : this.reactivePosition.value.name,
            cnt : this.reactivePosition.value.cnt,
            unit : this.reactivePosition.value.unit,
            price : this.reactivePosition.value.price,
            delivery : this.reactivePosition.value.delivery,
        };

        const req = this.serviceKp.CreatePosition(data);
        req.subscribe((res: any) => {
            this.reactivePosition.patchValue({
                name : null,
                cnt : 1,
                price : null,
                delivery : null,
            });

            this.data.sum += (data.cnt * data.price);

            this.positions.push(res);
        });

        return false;
    }

    /**
     * покажем форму редактирования позиции
     */
    ClickEditPosition(p: any) {
        this.reactivePositionUpdate.patchValue({
            id : p.id,
            name : p.name,
            cnt : p.cnt,
            unit : p.unit,
            price : p.price,
            delivery : p.delivery,
        });

        this.editPosition = p;
        return false;
    }

    SubmitSavePosition() {
        if (this.reactivePositionUpdate.invalid) {
            return false;
        }

        const data = {
            id : this.reactivePositionUpdate.value.id,
            name : this.reactivePositionUpdate.value.name,
            cnt : this.reactivePositionUpdate.value.cnt,
            unit : this.reactivePositionUpdate.value.unit,
            price : this.reactivePositionUpdate.value.price,
            delivery : this.reactivePositionUpdate.value.delivery,
        };

        const req = this.serviceKp.UpdatePosition(data);
        req.subscribe((res: any) => {
            this.editPosition.name = res.name;
            this.editPosition.cnt = res.cnt;
            this.editPosition.unit = res.unit;
            this.editPosition.price = res.price;
            this.editPosition.delivery = res.delivery;

            this.reactivePositionUpdate.patchValue({
                id : null,
            });
            this.editPosition = null;

            // пересчёт суммы
            let summ: number = 0;
            for (let i = 0; i < this.positions.length; i++) {
                summ += (this.positions[i].cnt * this.positions[i].price);
            }

            this.data.sum = summ;
        });


        return false;
    }

    /**
     * удаление позиции
     */
    ClickDeletePosition(p: any) {
        if (!confirm('Удалить текущую позицию?')) {
            return false;
        }

        const req = this.serviceKp.DeletePosition(p.id);
        req.subscribe(() => {
            const index = this.positions.indexOf(p);
            if (index !== -1) {
                this.positions.splice(index, 1);

                this.data.sum -= (p.cnt * p.price);
            }
        });
        return false;
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogKPComponent } from './kp.component';

describe('DialogKPComponent', () => {
  let component: DialogKPComponent;
  let fixture: ComponentFixture<DialogKPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogKPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogKPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

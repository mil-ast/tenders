import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogUpdateLotComponent } from './update-lot.component';

describe('DialogUpdateLotComponent', () => {
  let component: DialogUpdateLotComponent;
  let fixture: ComponentFixture<DialogUpdateLotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogUpdateLotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogUpdateLotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

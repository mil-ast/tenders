import {Component, Inject} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LotsService } from '../../_services/lots.service';
import { ManufacturersService } from '../../_services/manufacturers.service';
import { Lot } from '../../_classes/lot'; 
import * as moment from 'moment';

@Component({
    selector: 'dialog-update-lot',
    templateUrl: './update-lot.component.html',
    styleUrls: ['./update-lot.component.less']
})
export class DialogUpdateLotComponent {
    reactiveForm: FormGroup;
    manufacturers: string[] = [];

    constructor(
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<DialogUpdateLotComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Lot,
        private serviceLots: LotsService,
        private serviceManufacturers: ManufacturersService,
    ) {
		let date_end_auctions = null;
		if (data.date_end_auctions) {
			date_end_auctions = moment(data.date_end_auctions);
		}
		let date_review = null;
		if (data.date_review) {
			date_review = moment(data.date_review);
		}

		this.reactiveForm = this.fb.group({
			name: [data.name, [Validators.required, Validators.maxLength(65000)]],
			winner: [data.winner, Validators.maxLength(65000)],
			contacts: [data.contacts, Validators.maxLength(65000)],
			customer: [data.customer, Validators.maxLength(65000)],
			manufacturer: [data.manufacturer, Validators.required],
			number: [data.number, Validators.required],
            region: data.region,
            date_end_auctions : date_end_auctions,
            date_review : date_review,
            favorite : data.favorite === 'Y',
            url: [data.url, [Validators.pattern(/^(http|https):\/\/[^ "]+$/), Validators.maxLength(255)]],
			comment: [data.comment, Validators.maxLength(65000)],
			stage: data.stage
        });
        
        this.manufacturers = this.serviceManufacturers.Get();
    }

    Submit() {
		let date_end_auctions = null;
		if (this.reactiveForm.value.date_end_auctions) {
			date_end_auctions = this.reactiveForm.value.date_end_auctions.format('YYYY-MM-DD');
		}
		let date_review = null;
		if (this.reactiveForm.value.date_review) {
			date_review = this.reactiveForm.value.date_review.format('YYYY-MM-DD');
		}

        const data = {
            id : this.data.id,
            name : this.reactiveForm.value.name,
            winner : this.reactiveForm.value.winner,
            contacts : this.reactiveForm.value.contacts,
            customer : this.reactiveForm.value.customer,
            manufacturer : this.reactiveForm.value.manufacturer,
            number : this.reactiveForm.value.number,
            region : this.reactiveForm.value.region,
            date_end_auctions : date_end_auctions,
            date_review : date_review,
            favorite : this.reactiveForm.value.favorite ? 'Y' : 'N',
            url : this.reactiveForm.value.url,
            comment : (this.reactiveForm.value.comment||'').trim(),
            stage : this.reactiveForm.value.stage,
        };
            
        const req = this.serviceLots.Update(data);
        req.subscribe((res: any) => {
            this.data.UpdateLot(res);
        }, (err) => {
            console.error(err);
        });

        this.dialogRef.close(data)
        return false;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
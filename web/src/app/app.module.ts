import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, Injectable, forwardRef } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// сервисы
import { LotsService } from './_services/lots.service';
import { KpService } from './_services/kp.service';
import { ManufacturersService } from './_services/manufacturers.service';
import { FilterPipe } from './_pipes/filter.pipe';
import * as moment from 'moment';

import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatIconModule,
    MatDialogModule,
    MatSelectModule,
    MatSortModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatPaginatorIntl,
} from '@angular/material';

import { MomentDateAdapter} from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

export const DATE_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  }
};
moment.locale('ru');

import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
registerLocaleData(localeRu, 'ru');

import { AppComponent } from './app.component';
import { DialogCreateLotComponent } from './dialogs/create-lot/create-lot.component';
import { DialogUpdateLotComponent } from './dialogs/update-lot/update-lot.component';
import { DialogKPComponent } from './dialogs/kp/kp.component';
import { LotDescriptionComponent } from './lot-description/lot-description.component';
import { SwitchStageDateDirective } from './_directives/switch-stage-date.directive';

@Injectable()
export class MatPaginatorIntlRu extends MatPaginatorIntl {
    itemsPerPageLabel = 'Записей на страницу: ';

    getRangeLabel = (page: number, pageSize: number, length: number) => {
        let to = (page * pageSize) + pageSize;
        if (to > length) {
            to = length;
        }
        return ((page * pageSize) + 1).toString().concat(' - ', to.toString(), ' из ', length.toString());
    }
}

@NgModule({
    declarations: [
        AppComponent,
        DialogCreateLotComponent,
        DialogUpdateLotComponent,
        DialogKPComponent,
        LotDescriptionComponent,
        FilterPipe,
        SwitchStageDateDirective,
    ],
    imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        NoopAnimationsModule,
        MatButtonModule,
        MatTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatDialogModule,
        MatExpansionModule,
        MatSelectModule,
        MatSortModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatPaginatorModule,
    ],
    providers: [
        LotsService,
        KpService,
        ManufacturersService,
        {provide: LOCALE_ID, useValue: 'ru-RU'},
        {provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MatPaginatorIntl, useClass: forwardRef(() => MatPaginatorIntlRu)},
      ],
    bootstrap: [AppComponent],
    entryComponents: [
        DialogCreateLotComponent,
        DialogUpdateLotComponent,
        DialogKPComponent,
    ]
})
export class AppModule { }